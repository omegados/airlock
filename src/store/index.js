import { createStore } from "vuex";
import axios from 'axios';
axios.defaults.withCredentials = true;
axios.defaults.baseURL = process.env.VUE_APP_API_URL;

export default createStore({
  state: {
    user: null,
    auth: false
  },
  mutations: {
    SET_USER(state, user) {
      state.user = user;
      state.auth = Boolean(user);
    }
  },
  actions: {
    async logout( { dispatch }) {
      await axios.post("/logout");
      return dispatch("getUser");
    },
    async login( { dispatch }, credentials) {
      await axios.get("/sanctum/csrf-cookie");
      await axios.post("/login", credentials);
      return dispatch("getUser");
    },
    getUser( { commit } ){
      axios.get('/api/user')
        .then(res =>{
          // this.user = res.data;
          commit("SET_USER", res.data);
        })
        .catch(() => {
          commit("SET_USER", null);
        });
      }
  },
  modules: {}
});
